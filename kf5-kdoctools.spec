%global framework kdoctools

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:       kf5-%{framework}
Version:    5.116.0
Release:    1
Summary:    KDE Frameworks 5 Tier 2 addon for generating documentation

License:    GPLv2+ and MIT
URL:        https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  docbook-dtds
BuildRequires:  docbook-style-xsl

BuildRequires:  kf5-rpm-macros
BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-karchive-devel >= %{majmin}
BuildRequires:  kf5-ki18n-devel >= %{majmin}

BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel
%global _with_html --with-html
BuildRequires:  perl-generators
%global perl_uri_escape perl(URI::Escape)
BuildRequires:  %{perl_uri_escape}
BuildRequires:  qt5-qtbase-devel

Requires:       docbook-dtds
Requires:       docbook-style-xsl

%description
Provides tools to generate documentation in various format from DocBook files.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Provides:       kf5-kdoctools-static = %{version}-%{release}
Requires:       qt5-qtbase-devel
Requires:       %{perl_uri_escape}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5}  %{?tests:-DBUILD_TESTING:BOOL=ON}

%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name --with-man %{?_with_html}

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
make test -C %{_target_platform} ARGS="--output-on-failure --timeout 300" ||:
%endif

%ldconfig_scriptlets

%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf5_libdir}/libKF5DocTools.so.5*
## FIXME/TODO: which of these to move to -devel -- rex
%{_kf5_bindir}/checkXML5
%{_kf5_bindir}/meinproc5
%{_kf5_mandir}/man1/*.1*
%{_kf5_mandir}/man7/*.7*
%{_kf5_datadir}/kf5/kdoctools/
%if !0%{?_with_html:1}
%{_kf5_docdir}/HTML/*/kdoctools5-common/
%endif

%files devel
%{_kf5_includedir}/KDocTools/
%{_kf5_libdir}/libKF5DocTools.so
%{_kf5_libdir}/cmake/KF5DocTools/


%changelog
* Mon Nov 25 2024 peijiankang<peijiankang@kylinos.cn> - 5.116.0-1
- update to upstream version 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 04 2024 houhongxun <houhongxun@kylinos.cn> - 5.115.0-1
- update version to 5.115.0

* Thu Jan 04 2024 wangqia <wangqia@uniontech.com> - 5.113.0-1
- Update to upstream version 5.113.0

* Fri Aug 04 2023 zhangshaoning <zhangshaoning@uniontech.com> - 5.108.0-1
- Update to upstream version 5.108.0

* Mon Dec 12 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 5.100.0-1
- Update to upstream version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 loong_C <loong_c@yeah.net> - 5.95.0-1
- update to upsttream version 5.95.0

* Sat Feb 12 2022 tanyulong <tanyulong@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Thu Jan 13 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Thu Nov 5 2020 douyan <douyan@kylinos.cn> - 5.55.0-2
- remove perl(Any::URI::Escape) dependency

* Thu Jul 23 2020 wangmian <wangmian@kylinos.cn> - 5.55.0-1
- Init kf5-kdoctools project
